global start
extern long_mode_start

section .text
bits 32
start:
  mov esp, stack_top

  call check_multiboot
  call check_cpuid
  call check_long_mode

  call set_up_page_tables
  call enable_paging

  lgdt [gdt64.pointer]

  jmp gdt64.code:long_mode_start

set_up_page_tables:
  ; map first P4 entry to P3 table
  mov eax, p3_table
  or eax, 0b11
  mov [p4_table], eax

  ; map first P3 entry to P2 table
  mov eax, p2_table
  or eax, 0b11
  mov [p3_table], eax

  ; map each P2 entry to a 2MiB page
  mov ecx, 0
  call .map_p2_table

  ret

.map_p2_table:
  ; map ecx-th P2 entry to a page that starts at address 2MiB*ecx
  mov eax, 0x200000   ; 2MiB
  mul ecx             ; ecx-th page = ecx * eax
  or eax, 0b10000011  ; set present, writable and huge page bits
  mov [p2_table + ecx * 8], eax ; map ecx-th entry

  inc ecx
  cmp ecx, 512 ; if counter == 512, the P2 table has been mapped
  jne .map_p2_table ; else loop over and map next entry

  ret

; NOTE: see https://en.wikipedia.org/wiki/Physical_Address_Extension
enable_paging:
  ; load P4 to cr3 register (CPU needs it to access P4)
  mov eax, p4_table
  mov cr3, eax

  ; enable PAE-flag in cr4
  mov eax, cr4
  or eax, (1 << 5)
  mov cr4, eax

  ; set the long mode bit in the EFER MSR
  mov ecx, 0xC0000080
  rdmsr
  or eax, (1 << 8)
  wrmsr

  ; enable paging in the cr0 register
  mov eax, cr0
  or eax, (1 << 31)
  mov cr0, eax

  ret

; NOTE: see https://wiki.osdev.org/Setting_Up_Long_Mode#Detection_of_CPUID
;   CPUID is supported if we can flip the ID bit in the FLAGS register.
check_cpuid:
  pushfd
  pop eax
  mov ecx, eax
  xor eax, 1 << 21
  push eax
  popfd
  pushfd
  pop eax
  push ecx
  popfd
  cmp eax, ecx
  je .no_cpuid
  ret
.no_cpuid:
  mov al, "1"
  jmp error

; NOTE: see https://wiki.osdev.org/Setting_Up_Long_Mode#x86_or_x86-64
;   and more details at https://en.wikipedia.org/wiki/CPUID#EAX.3D80000001h:_Extended_Processor_Info_and_Feature_Bits.
;   First check if function that determines whether long mode is available,
;   then use this function to detect long mode if it exists.
check_long_mode:
  mov eax, 0x80000000
  cpuid
  cmp eax, 0x80000001
  jb .no_long_mode

  mov eax, 0x80000001
  cpuid
  test edx, 1 << 29
  jz .no_long_mode
  ret
.no_long_mode:
  mov al, "2"
  jmp error

check_multiboot:
  cmp eax, 0x36d76289
  jne .no_multiboot
  ret
.no_multiboot:
  mov al, "0"
  jmp error

error:
  mov dword [0xb8000], 0x4f524f45
  mov dword [0xb8004], 0x4f3a4f52
  mov dword [0xb8008], 0x4f204f20
  mov byte [0xb800a], al
  hlt

section .rodata
; NOTE: GDT used for Segmentation - see https://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf
gdt64:
  dq 0
.code: equ $ - gdt64
  dq (1<<43) | (1<<44) | (1<<47) | (1<<53)
.pointer:
  dw $ - gdt64 - 1
  dq gdt64

section .bss
align 4096
p4_table:
  resb 4096
p3_table:
  resb 4096
p2_table:
  resb 4096
stack_bottom:
  resb 4096 * 4
stack_top:
