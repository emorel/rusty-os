#![no_std]
#![feature(panic_info_message)]
#![feature(fmt_as_str)]

mod vga_buffer;

use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    println!("Welcome To RustyOS !");
    println!();
    println!("Booting...");
    println!();

    let messages = [
        "Loading CPU...",
        "Loading GPU...",
        "Loading lorem ipsum...",
        "Loading some more stuff...",
    ];

    for (i, message) in messages.iter().enumerate() {
        println!("{}", message);
        pretend_doing_lots_of_stuff(i as isize);
    }

    loop {
        println!("...");
        pretend_doing_lots_of_stuff(10);
    }
}

fn pretend_doing_lots_of_stuff(i: isize) -> () {
    for _ in 0..=(100000 * (i * 10)) {}
    return;
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}
