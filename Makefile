arch ?= x86_64
kernel := build/kernel-$(arch).bin
iso := build/os-$(arch).iso

target_dir ?= $(arch)-kernel
target ?= debug
rusty_os := target/$(target_dir)/$(target)/librusty_os.a

linker_script := src/arch/$(arch)/linker.ld
grub_cfg := src/arch/$(arch)/grub.cfg
asm_sources := $(wildcard src/arch/$(arch)/*.asm)
asm_objects := $(patsubst src/arch/$(arch)/%.asm, build/arch/$(arch)/%.o, $(asm_sources))

.PHONY: all clean run iso kernel/release kernel/debug

all: $(kernel)

clean:
	@rm -r build/

run: $(iso)
	@qemu-system-x86_64 -cdrom $(iso) -D ./log/qemu.log -monitor stdio -d int -d cpu_reset

iso: $(iso)

kernel/release:
	@cargo build --release

kernel/debug:
	@cargo build

$(iso): $(kernel) $(grub_cfg)
	@mkdir -p build/isofiles/boot/grub
	@cp $(kernel) build/isofiles/boot/kernel.bin
	@cp $(grub_cfg) build/isofiles/boot/grub
	@grub-mkrescue -o $(iso) build/isofiles 2> /dev/null
	@rm -rf build/isofiles

$(kernel): kernel/$(target) $(rusty_os) $(asm_objects) $(linker_script)
	@ld -n -T $(linker_script) -o $(kernel) $(asm_objects) $(rusty_os)

build/arch/$(arch)/%.o: src/arch/$(arch)/%.asm
	@mkdir -p $(shell dirname $@)
	@nasm -felf64 $< -o $@
