FROM rustlang/rust:nightly

RUN apt-get update && apt-get -y install nasm xorriso grub
RUN rustup component add rust-src
