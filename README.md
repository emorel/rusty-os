# Rusty OS - A Minimal x86 Kernel in Rust

![Hello World!](images/kernel00.png)

# Installation

> **Note**  
> Rusty OS only support x86 CPU and will also fail to boot on older CPUs.

All the latest versions of the ISO images are available for download in the release sections.

The released ISO images should be bootable through USB or CD. You may alternatively boot them through your favored VM manager.

# Development

```bash
git clone git@gitlab.com:emorel/rusty-os
```

## Requirements

The project requires a few system dependencies in order to compile :

- a Rust compiler - the `rustup` toolchain is *highly* recommended
- an x86 Assembler - `nasm` is recommended
- a GRUB image builder - `grub-mkrescue` is recommended

> **Note**  
> If you are using `grub-mkrescue`, you must additionally install `xorriso`.

Finally, Rusty OS runs bare metal and needs to compile Rust itself, but doing so requires adding the standard library source code :

```bash
rustup component add rust-src
```

## Building a bootable ISO

Once all dependencies installed, you should have all the necessary tools to compile, link all sources and build a bootable ISO.

> **Note**  
> These examples assume you installed the *recommended* libraries.

### 1. Compiling the bootloader

First, you need to compile the bootloader ASM files with `nasm`.

```bash
mkdir -p build/arch/x86_64

# Compile the bootloader files
nasm -felf64 src/arch/x86_64/multiboot_header.asm  -o build/arch/x86_64/multiboot_header.o
nasm -felf64 src/arch/x86_64/boot.asm -o build/arch/x86_64/boot.o
nasm -felf64 src/arch/x86_64/long_mode_init.asm -o build/arch/x86_64/long_mode_init.o
```

### 2. Compiling Rusty OS

If the project was properly cloned, all Cargo-related configuration should already be set up, simply run :  

```bash
cargo build
```

If for some reason you are missing the Cargo configuration, you need to define a `x86_64-kernel` build target, instruct Cargo to compile `core` and `compiler_builtins`, and enable memory-related built-ins :

```toml
# .cargo/config.toml
[unstable]
build-std = ["core", "compiler_builtins"]
build-std-features = ["compiler-builtins-mem"]

[build]
target = "x86_64-kernel.json"
```

```json
// x86_64-kernel.json
{
  "llvm-target": "x86_64-unknown-none",
  "data-layout": "e-m:e-i64:64-f80:128-n8:16:32:64-S128",
  "arch": "x86_64",
  "target-endian": "little",
  "target-pointer-width": "64",
  "target-c-int-width": "32",
  "os": "none",
  "linker-flavor": "ld.lld",
  "linker": "rust-lld",
  "panic-strategy": "abort",
  "features": "-mmx,-sse,+soft-float",
  "disable-redzone": true,
  "executables": true
}
```

After setting up this configuration, you should be able to run the build command successfully.

### 3. Linking

In order to link the files into a single binary file, you need to run the following command :

```bash
# Link the bootloader object files and the compiled Rust static library
ld -n -T src/arch/x86_64/linker.ld -o build/kernel-x86_64.bin \
  build/arch/x86_64/long_mode_init.o \
  build/arch/x86_64/boot.o \
  build/arch/x86_64/multiboot_header.o \
  target/x86_64-kernel/debug/libkernel.a
```

### 4. Building the ISO

Once all files linked, you can set up the directory structure for `grub-mkrescue` and generate a bootable ISO image :

```bash
# Set up the files
mkdir -p build/isofiles/boot/grub
cp build/kernel-x86_64.bin build/isofiles/boot/kernel.bin
cp src/arch/x86_64/grub.cfg build/isofiles/boot/grub

# Build the image
grub-mkrescue -o build/os-x86_64.iso build/isofiles 2> /dev/null

# Clean up unnecessary files
rm -rf build/isofiles
```

Running this script will create an ISO located at `build/os-86_64.iso`.

### 5. Running the OS

You may burn the generated ISO on a CD/ROM or a bootable USB stick to install the OS on real hardware.

For development purposes, it is recommended to use a VM manager, such as `qemu` :

```bash
qemu-system-x86_64 -cdrom build/os-x86_64.iso
```
## Using Make recipes

Optionally, if you wish to use the Makefile recipes to make the building much less time consuming, you need to install the following dependencies :

- `make` - for obvious reasons
- `qemu` is used for running the kernel in a bare metal emulation
  - in order to emulate an x86 architecture, you need `qemu-system-x86_64`

After this, you can use the recipes :

```bash
# Compiles, links, builds the ISO and runs the OS in Qemu
make run

# Compiles, links and builds the ISO
make iso

# Compiles the Rust code
make kernel
```

Additionally, you can pass the `target=[debug|release]` flag to create a debug or release build. By default, the target is `debug`.  
